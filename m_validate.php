<?php

// If someone tries to run this file stand-alone, exit with no hint of what's gone wrong.
if (!defined('ISITSAFETORUN')) {
    die('');
}

$valid = TRUE; // Set this to FALSE if any of the validations fail

$formerror['client'] = '';
$formerror['date'] = '';
$formerror['amount'] = '';
    
/* Some of these are simple checks to see if not empty, others are more complex.
 * Although it seems inefficient, we are dealing with each one in turn in case additional
 * complexity is required at a later date. 
*/
if (isset($webdata['client'])) {
    if (! preg_match("/^[A-Za-z ']+$/",$webdata['client'])) {
        echo "<p>Checking client</p>";
        $formerror['client'] = '<span class="text-danger">Not valid. Only letters, space and apostrophe allowed.</span>';
        $valid = FALSE;
    }
} else {
    $valid = FALSE;
}

if (isset($webdata['amount'])) {
    if (! preg_match("/^[0-9]+$/",$webdata['amount'])) {
        echo "<p>Checking amount</p>";
        $formerror['amount'] = '<span class="text-danger">Not valid. Only numbers allowed.</span>';
        $valid = FALSE;
    }
} else {
    $valid = FALSE;
}

if (isset($webdata['date'])) {
    if (! preg_match("/^[0-9]+$/",$webdata['date'])) {
        echo "<p>Checking date</p>";
        $formerror['date'] = '<span class="text-danger">Not valid. Only numbers allowed.</span>';
        $valid = FALSE;
    }
} else {
    $valid = FALSE;
}

if (isset($webdata['prereqs'])) {
    if (strcmp($webdata['prereqs'],'') == 0 ) {
        echo "<p>Checking prereqs</p>";
        $formerror['prereqs'] = '<span class="text-danger">Not valid. Cannot be blank - enter "None" if no pre-requsites.</span>';
        $valid = FALSE;
    }
}

//sleep (5);
// Toggle this for testing
//$valid = FALSE;

?>
