<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Database Utility</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style>
    div {color: red; padding: 10px; border-style: solid; border-color: cornflowerblue;}
</style>
</head>
<body>
<div class="container">
<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    // Define a constant allowing this script to run
    define('ISITSAFETORUN', TRUE);
    // Include our database connection functions
    include 'mydatabase.php';
    include 'helpers.inc.php';

    $dbhandle = mysqli_connect($hostname, $username, $password)
        or die( "Unable to connect to MySQL");

    $selected = mysqli_select_db($dbhandle, $mydatabase) or die("Unable to connect to " . $mydatabase );
    $sql = "SELECT * FROM sales where amount <> 0 ORDER BY client";
    $result = mysqli_query($dbhandle,$sql) or die ("Could not execute the query " . $sql );

    echo '<ul class="list-group">';

    while ($row = mysqli_fetch_array($result))
    {
        echo '<li class="list-group-item">';
        htmlout($row['client']);
        echo  " - ";
        htmlout($row['date']);
        echo " | ";
        htmlout($row['amount']);
        echo "</li>";
    }


    echo '</ul>';

?>
</div>
</body>
</html>