<?php

/* 
 * m_formdata.php - a PHP only script that gathers the POST data for processing
 * Data is added to an array called $webdata.
 */

// If someone tries to run this file stand-alone, exit with no hint of what's gone wrong.
if (!defined('ISITSAFETORUN')) {
    die('');
}

// If $_POST is not empty, then gather all the data and stick it in our own array for processing as required.
if (!empty($_POST)) {
    foreach ($_POST as $key => $value) {
        // Treat checkboxes as special case as they are sent as an array
        if (is_array($value)) {
            foreach ($value as $cbkey => $cbvalue) {
                //echo "<p>Key =  {$key}.  Array = ${cbkey}, Value = {$cbvalue} </p>";
                $webdata[$key.$cbkey] = $cbvalue;
            }            
        } else {
            //echo "<p>Key =  {$key}.  Value = {$value} </p>";
            $webdata[$key] = $value;
        }
    }
} else {
    // $_POST is empty
    $webdata['none'] = 'none';
}

?>
